package p6hiline;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

public class GameOver {
	
	static boolean vastus;
	
    public static boolean display(String title, String message) {
        Stage window = new Stage();

        //Block events to other windows
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);

        Label label = new Label();
        label.setText(message);
        
        
        //Create two buttons
        Button yesButton = new Button("Jah!");
        Button noButton = new Button("Ei!");

        //Clicking will set answer and close window
        yesButton.setOnAction(e -> {
        	vastus = true;
            window.close();
        });
        noButton.setOnAction(e -> {
        	vastus = false;
            window.close();
        });

        
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, yesButton, noButton);
        layout.setAlignment(Pos.CENTER);

        //Display window and wait for it to be closed before returning
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        
        
        return vastus;
    }

}
