package p6hiline;

/*
 * 
 *  
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class P6hi extends Application {

	Stage window;
	Scene scene1, scene2, scene3;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) {
		window = primaryStage;

		
		// Button 1
		Label label1 = new Label("Tere tulemust mängu nimega 'Peeter'");
		Button button1 = new Button("Alusta");
		button1.setOnAction(e -> window.setScene(scene3));

		// Layout 1 - children laid out in vertical column
		VBox layout1 = new VBox(20);
		layout1.getChildren().addAll(label1, button1);
		scene1 = new Scene(layout1, 200, 200);

		// Button 2
		Button button2 = new Button("Valmis");
		button2.setOnAction(e -> {
			boolean result = GameOver.display("Mäng 'Peeter'", "Mängi uuesti?");

			if (result == true) {
				window.setScene(scene1);
			} else {

				System.out.println("Mäng saigi läbi");
				System.exit(0);
			}

		});

		// Tegelase scene
		Group root = new Group();
		Canvas canvas = new Canvas(800, 600);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		Joonistaja tegelane1 = new Joonistaja();
		tegelane1.joonistaBody(gc);

		Joonistaja tegelane2 = new Joonistaja();
		tegelane2.joonistaPea(gc);


		// Klouni kostüüm

		Joonistaja kloun = new Joonistaja();
		Button button3 = new Button("Kloun");
		button3.setOnAction(e -> {
			kloun.joonistaKloun(gc);
			tegelane2.joonistaPea(gc);
		});
		
		// Santa kostüüm
		
		Joonistaja santa = new Joonistaja();
		Button button4 = new Button("Santa");
		button4.setOnAction(e -> {
			tegelane2.joonistaPea(gc);
			santa.joonistaSanta(gc);
		});
		
		
		
		
		VBox layout2 = new VBox(20);
		layout2.getChildren().addAll(button2, button3);

		VBox vbox = new VBox(8); // spacing = 8
		vbox.getChildren().addAll(button2, button3, button4);

		SplitPane sp = new SplitPane();
		sp.getItems().addAll(vbox, canvas);
		scene3 = new Scene(sp);
		window.setScene(scene3);

		// Display scene 1 at first
		window.setScene(scene1);
		window.setTitle("Mäng 'Peeter'");
		window.show();
	}

}
