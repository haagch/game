package kostyymid;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Santa {

	public void joonista(GraphicsContext gc) {

		Santa santa = new Santa();

		santa.joonistaKeha(gc);
		santa.joonistaJuuksedV(gc);
		santa.joonistaJuuksedP(gc);
		santa.joonistaHabe(gc);
		santa.joonistaBlush(gc);
		santa.joonistaMyts(gc);
		santa.joonistaVunts(gc);
		santa.joonistaNina(gc);
	}

	private void joonistaKeha(GraphicsContext gc) {
		// Keha
		gc.setFill(Color.RED);
		gc.fillRoundRect(75, 260, 250, 250, 20, 20);
	}

	private void joonistaHabe(GraphicsContext gc) {
		// habe 
		gc.setFill(Color.LIGHTGRAY);
		double xpoints[] = { 110, 290, 200 };
		double ypoints[] = { 190, 190, 360 };
		int num = 3;
		gc.fillPolygon(xpoints, ypoints, num);
		gc.setStroke(Color.WHITE);
		gc.strokePolygon(xpoints, ypoints, num);
		
	}

	private void joonistaJuuksedV(GraphicsContext gc) {

		// juuksed vasak pool
		gc.setFill(Color.LIGHTGRAY);
		gc.fillOval(100, 55, 50, 50);
		gc.fillOval(90, 80, 30, 30);
		gc.fillOval(115, 85, 35, 30);
		gc.setStroke(Color.WHITE);
		gc.strokeOval(100, 55, 50, 50);
	}

	private void joonistaJuuksedP(GraphicsContext gc) {
		// Juuksed parem pool
		gc.setFill(Color.LIGHTGRAY);
		gc.fillOval(250, 55, 50, 50);
		gc.fillOval(275, 80, 30, 30);
		gc.fillOval(250, 85, 35, 30);
		gc.setStroke(Color.WHITE);
		gc.strokeOval(250, 55, 50, 50);

	}

	private void joonistaMyts(GraphicsContext gc) {
		
		// Myts

		
		gc.setFill(Color.RED);
		double xpoints[] = { 110, 290, 200 };
		double ypoints[] = { 62, 62, 0 };
		int num = 3;
		gc.fillPolygon(xpoints, ypoints, num);
		gc.setFill(Color.WHITE);
		gc.fillRoundRect(90, 60, 220, 40, 60, 60);
		gc.setStroke(Color.LIGHTGRAY);
		gc.strokeRoundRect(90, 60, 220, 40, 60, 60);
		
		gc.setFill(Color.WHITE);
		gc.fillOval(190, 0, 20, 15);
		gc.setStroke(Color.LIGHTGRAY);
		gc.strokeOval(190, 0, 20, 15);
	}

	private void joonistaBlush(GraphicsContext gc) {
		// Blush
		gc.setFill(Color.RED);
		gc.fillOval(110, 139, 35, 35);
		gc.fillOval(255, 139, 35, 35);

	}
	
	private void joonistaVunts1(GraphicsContext gc){
		//funtsi vasakpool
		gc.setFill(Color.LIGHTGRAY);
		double xpoints[] = { 100, 110, 200, 200};
		double ypoints[] = { 150, 190, 170, 180};
		int num = 4;
		gc.fillPolygon(xpoints, ypoints, num);
		gc.setStroke(Color.WHITE);
		gc.strokePolygon(xpoints, ypoints, num);
		
		gc.setFill(Color.LIGHTGRAY);
		double xpoints2[] = { 300, 290, 200, 200};
		double ypoints2[] = { 150, 190, 170, 180};
		int num2 = 4;
		gc.fillPolygon(xpoints2, ypoints2, num2);
		gc.setStroke(Color.WHITE);
		gc.strokePolygon(xpoints2, ypoints2, num2);
		
	}
	
	private void joonistaVunts(GraphicsContext gc){
		
		gc.setFill(Color.LIGHTGRAY);
		gc.fillRoundRect(100,165,100, 20, 60, 60);
		gc.fillRoundRect(200,165,100, 20, 60, 60);
	}
	
	private void joonistaNina(GraphicsContext gc) {

		// See on nina
		gc.setFill(Color.RED);
		gc.fillOval(182, 139, 35, 35);
	}
}
